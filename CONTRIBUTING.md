# Contributors' Guide to Bash Scripts Templates

## TL;DR (Too Long; Didn't Read)
To make sure we keep our software production safely in building those scripts while you help us grow our collection, an few things you need to follow.

* Get a GitLab.com account and verify your Git commiter emails to your account.
* Sign your commits with GPG because we hate source code forgery. (Don't forget to verify them manually, just making sure things are trusted..)
* Avoid commiting your deepest secrets, even personal ones IRL.
* When working locally, sing-off also your commits.

## The Legalese Version
This document contains

### Policy #1: Get a GitLab.com account and verify your commiter emails to your account
To make sure that your contributions are authentic, you must create an GitLab.com account and verify your commiter email address you use in Git to confirm that you're have an GitLab.com account. If you commiting from GitHub and mirroring your forks, please pair it first.

### Policy #2: Sign your commits with GPG because we hate source code forgery.
To just be sure that you are the authentic commiter, you need some GPG keys added to your account

If you commiting using GitLab's web interface (or their Web IDE), we don't required it because GitLab will handle it with you.

> P.S.: Even some random intruder can rebuild private keys from these public ones, it's better to keep your GPG keys have stronger passwords.

### Policy #3: Avoid commitng your deepest secrets, even personal ones IRL.
> Also on our handbook: [Making your deepest secrets private]

### Policy #3: When working locally, sign-off also your commits.

To sign-off your commits, use the following command.
```bash
## Think you enabled GPG siging by default in your global settings.
git commit --singoff
```


[Making your deepest secrets private]: https://en.handbooksbythepins.gq/the-gitlab-way/your-deepest-secrets-are-worst-too
