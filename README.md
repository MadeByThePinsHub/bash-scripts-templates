![Project Maintenance](https://img.shields.io/maintenance/yes/2020?style=for-the-badge) ![Made with Love](https://img.shields.io/badge/Made%20with-love-red?style=for-the-badge)
# Bash Scripts Templates

This repository is where our team creates and complies templates of different bash scripts mostly for both **Linux** and **macOS**.

## Requirements
* Linux computer (VM is okay)
* Supports `sh` or `bash`
* Additional dependencies may vary on template you use.

## Using the templates
### Working Locally or Just Experimenting Things
To get started, all you need is an Linux (or macOS) computer (or even an VM) and Git. Some scripts under `templates/<category>/*` requires some additional work. Most templates are tested in built and tested in Linux distros virtual machines, among with [community testers](Community Script Testers.md).

The first step is clone the whole Git repoistory, which requires `git` package installed on your Linux distro.
```bash
# Go to your home directory
cd $HOME

# Install Git first in CLI on our Ubuntu VM. Installation varies on other Linux distros.
$ sudo apt-get install git

# Verify the Git installation.
$ git --version

# Now, clone and hit the road!
$ git clone https://gitlab.com/MadeByThePinsTeam-DevLabs/bash-scripts-templates.git && cd bash-scripts-templates
```

After cloning the whole repository, let get things started.
```bash
## Navigate to the local repo's templates dir.
cd bash-scripts-temps

## Get the whole list of directories.
ls -a templates

## Select what category you want to try. We're using the Hello World script for this tutorial. Use `&&` and then type ls -a.
cd hello-world && ls -a

## Choose helo-world-docker_ce script to install Docker on your system.
bash hello-world-docker_ce.sh

## If you are have Docker installed, we'll check whetever its installed successfully
bash $HOME/bash-scripts-templates/templates/docker/postinstall-hello-world.sh
```
### With the CLI
```bash
# Install the CLI using the installer script.
curl -sL https://bash-script-samples-api.glitch.me/install/cli-dev | bash - 

## Try it now
BashScriptSamplesCLI --version
```

## Contributing
See `CONTRIBUTING.md` for details on how you can contribute to our collection of Bash scripts.

## License and Copyright
Copyright (c) 2020 by Andrei Jiroh and The Pins Team, released under GPL 3.0 or later. Community contributions are also licensed under the same license unless otherwise specified.

If you use any of the scripts in this repository, you must **discolse the source code**, **keep the liense and copyright notices**, **use GPL v3 or later** and **state any changes**, as per license terms and conditions. See the `LICENSE.md` file for more information.
