#!/bin/sh

## In lines 3 to 33, we define some stuff for this script to work.
print_status() {
    echo
    echo "[INFO] $1"
    echo
}

welcomeMessage() {
    echo "########################################"
    echo "Wiki.js Installer for Glitch"
    echo
    echo "You are about to install Wiki.js in Glitch, which will nuke everything in your project's direcotry. Please make sure you want to make this project your Wiki.js instance."
    echo 
    echo "After instlling Wiki.js, you can update your instance using this script available soon: https://gitlab.com/MadeByThePinsTeam-DevLabs/bash-scripts-templates/-/raw/master/templates/glitch/wiki.js/update.sh"
    echo "########################################"
    echo
}

promptToContinue() {
    read -n 1 -s -r -p "[PROMPT] Press any key to continue or Ctrl+C to abort the installation process."
}

bail() {
    echo '[ERR] Command executing failed, I am now quiting!'
    exit 1
}

exec_cmd_nobail() {
    echo "+ $1"
    bash -c "$1"
}

exec_cmd() {
    exec_cmd_nobail "$1" || bail
}

## Prompt users to enter any key to continue.
welcomeMessage
promptToContinue

## Nuke everything in Glitch, including any local Git data.
print_status "Nuking everything in your Glitch project in 15 seconds..."
sleep 15
exec_cmd "rm -rf .config .data .env .git .glitch-assets package.json public/ README.md server.js shrinkwrap.yaml views/"

## Then, do some Node modules nuking by removing packages we don't needed.
print_status "Doing extra Node.js modules nuking, please wait..."
exec_cmd "pnpm prune"

## Pull the latest Wiki.js release from GitHub
print_status "Getting the latest release from Wiki.js repo..."
exec_cmd_nobail "wget https://github.com/Requarks/wiki/releases/download/2.3.81/wiki-js.tar.gz"

## Next, extract everything, but not Node modules included in tarballs.
print_status "Extracting the tarballs..."
exec_cmd_nobail "tar xzf wiki-js.tar.gz --exclude node_modules"

## After extraction process, move the Wiki.js tarballs to /dev/null.
## P.S.: Not literrally moving them to /dev/null, just removing them the usual way.
print_status "Nuking the Wiki.js tarballs..."
exec_cmd_nobil "rm -rf wiki-js.tar.gz"

## Install production-grade packages and rebuild your SQLite3.
print_status "Installing production-grade packages..."
exec_cmd "pnpm install -P"
print_status "Rebuilding SQLite 3..."
exec_cmd "pnpm rebuild sqlite3"

## Nuke the sample configuration file as we pull the one made by @ihack2712 in GitHub Gist.
print_status "Nuking the smple configuration file..."
exec_cmd_nobail "rm config.sample.yml"

## Now, get that config.yml from GitHub Gist.
print_status "Getting the available configuration file from GH Gists for Glitch..."
exec_cmd "wget https://gist.githubusercontent.com/ihack2712/fa6eb022a5b3390972bcd0fb015aab8b/raw/d66df5d4c7c0376ea254c8a8fbed0c367e6f20ea/config.yml"

## Finalize more stuff.
print_status "Adding the '/app/.data' directory..."
exec_cmd_nobail "mkdir .data"

## Confirm we're using Glitch, otherwise quit.
if [ ! -f /usr/bin/refresh ]; Then
    echo "[ERR] Can't do a force refresh because you're outside of Glitch."
    echo "[ERR]"
    echo "[ERR] To launch, try `npm start` manually later."
    echo "[INFO] Exiting..."
    exit 1
else
    echo "[SUCESS] Installation done, refreshing..."
    refresh
    exit
fi
