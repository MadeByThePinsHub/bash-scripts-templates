# Wiki.js in Glitch?
Yes, you can install Wiki.js in Glitch with a SQLite 3 database in your Glitch containner.

## Installation
Want to get you started?

1. [Remix Glitch's `hello-express` project by clicking here.](https://glitch.com/edit/#!/remix/hello-express)
2. Make your newly-remixed project private, so no one can see your secrets about your Wiki.js instance.
3. Change your project slug before continuing. Once you completely installed Wiki.js, **YOU WILL NOT ABLE TO CHANGE THIS LATER**.
4. In a new tab, go to `glitch.com/edit/console.html?your-project-here` to launch a full-screen console.
5. Copy this code and paste it to that tab and hit enter.
```sh
## Copy this snippet below to kick the installer.
curl -o- https://gitlab.com/MadeByThePinsTeam-DevLabs/bash-scripts-templates/-/raw/master/templates/glitch/wiki.js/install.sh | bash
```
6. Cross your finger as you wait. Monitor your app's resources too!
7. After that, open your Glitch app in a new tab and complete the setup wizard.
8. Volia! No more step 8. Now, enjoy your Wiki.js instance in Glitch.

## Updating
We'll detail that soon as updating 
