# Node.js Scripts
You are browsing our collection of Node.js bash scripts, from installation of the framework to

## The Files

### installation
> Your Linux distro must support Debs and RPMs, otherwise [read th docs](https://github.com/nodesource/distributions/blob/master/README.md). There is also scripts for installing Node.js without using `sudo`.

To install Node.js using our `deb_install_Yx.sh` (Y represents major version number) or `rpm_install.Yx.sh` (coming soon), you need your system adminstrator's assistance and/or have allowed your `sudo` usage.[^1] [^2]

[^1]: In Debian, you need to run these scripts as `root`. Please proceed at your own risk!
[^2]: There is an alternatie scripts for users who prefer to install Node.js as `root`, just look for `-alt` in the files.

```bash
## Let's say you are now on the root of this repo's local copy on your computer running Debian and its derivatives.
dumbbell@maglubay-mga-dumbbell:~/bash-script-templatess$ bash ./templates/nodejs/deb_install_12x.sh

## That script contains `sudo`, so you need either have admin permissions or let your sysadmin do it for you.
```

#### Zero Sudo Scripts
Our zero-sudo scripts will install Node Version Manager first then installs latest Node.js version in LTS.
```bash
dumbbell@maglubay-mga-dumbbell:~/bash-scripts-templates$ bash ./templtes/nodejs/userInstall_nvm.sh
```

## Testing Installations
To test, run the `deb_testInstallations.sh` script. This should be take less than a minute.

If you want to have logs for you, we have the `customizedInstallationTest.sh` file if you want to get the logs for you.
