#!/bin/sh

intro() {
    echo "#########################"
    echo ""
    echo "${bold}Running test suites for your installation."
    echo ""
    echo "Please standby as we're been testing scripts for you."
    echo ""
    echo "#########################"
    echo ""
    echo "To abort, press Ctrl+D."
    echo ""
    sleep 5
}

print_status() {
    echo ""
    echo "## $1"
    echo ""
}

intro

## Get the test script and run it.
print_status "Downloading test scripts from official website..."
curl -sL https://deb.nodesource.com/test | bash -
