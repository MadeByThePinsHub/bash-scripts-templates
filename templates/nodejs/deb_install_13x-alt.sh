#!/bin/sh

success_install() {
    echo "${bold}Congratulations!${normal}Your current Node.js version installed is $(node -v)."
}

## WARNING: REQUIRES ROOT USER!
## This alternative script requires access to the root user,
## which login are disabled by default in Debian and Ubuntu.
## Procced at your OWN RISK when using root user.

## Add the NodeSource Nodejs LTS builds for 12.x repo
curl -sL https://deb.nodesource.com/setup_13.x | bash -
echo ""
sleep 5

## Next, install dev tools for building native addons
apt-get install -y gcc g++ make
echo ""
sleep 5

## Then, install Yarn
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
apt-get update -y && apt-get install -y yarn
echo ""
sleep 5

## Finally, hit the road!
apt install Nodejs
echo ""
sleep 3
sucess_install
