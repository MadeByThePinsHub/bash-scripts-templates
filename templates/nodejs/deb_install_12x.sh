#!/bin/sh

## Add the NodeSource Nodejs LTS builds for 12.x repo
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
echo ""
sleep 5

success_install() {
    echo "${bold}Congratulations!${normal}Your current Node.js version installed is $(node -v)."
}

## Next, install dev tools for building native addons
sudo apt-get install -y gcc g++ make
echo ""
sleep 5

## Then, install Yarn
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update -y && sudo apt-get install -y yarn
echo ""
sleep 5

## Finally, hit the road!
sudo apt install Nodejs
echo ""
sleep 3
success_install
