#!/bin/sh

welcomeScript() {
  echo "##################################################"
  echo "Welcome to the installation script for VirtualBox Additions!"
  echo
  echo "Follow the prompts as the script progresses."
  echo
  echo "##################################################"
}

print_status() {
    echo "[INFO] $1"
}

print_standby() {
    echo "[STANDBY] $1"
}

print_success() {
    echo "[SUCCESS] $1"
}

mountWarn() {
  echo "##################################################"
  echo "We need to manually mount Guest Additions ISO files to your server."
  echo
  echo "Make sure you inert the Guest Additions CD within 90 seconds. Otherwise, the script will be aborted."
  echo
  echo "##################################################"
  sleep 90
}

## Install dependencies
print_status "Installing dependencies..."
sudo apt-get install -y dkms build-essential linux-headers-generic linux-headers-$(uname -r)
print_success 'Dependencies installed!'

## Mount the Guest Additions ISO CD ROM drive
print_sandby "Waiting for the Guest Additions ISO..."
mountWarn
