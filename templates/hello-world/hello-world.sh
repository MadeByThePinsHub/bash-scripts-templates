#!/bin/sh

###################################
# What is this bash script again? #
# This bash script is called      #
# Hello World. Why? It's prints an#
# "Hello World" message to your   #
# terminal.                       #
# =============================== #
# Feel free to edit the script as #
# what do you want.               #
###################################

# Prints "Hello, world" to your terminal
echo "Hello, world!"

# Prints the same sentence, but with your UNIX username.
echo "Hello world, $USER!"
