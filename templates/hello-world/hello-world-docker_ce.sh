#!/bin/sh

# Bash Scripts Templates - Hello World, Docker for Ubuntu!
# Copyright (C) 2020  Andrei Jiroh Halili
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

####################################
# What is this bash script again?  #
#                                  #
# This Hello World bash script     #
# demonstrates how we install      #
# Docker CE in rootless mode and   #
# install the hello-world image on #
# an Ubuntu computer.              #
# ================================ #
# Feel free to edit thie script as #
# what do you want.                #
####################################

## Declutter the mess in CLI.
echo "Decluttering your console logs..."
clear

## Welcome message
echo "####################################"
echo ""
echo "Welcome to Docker CE installation script!"
echo ""
echo "This script will help you install Docker and cnfirm wherever this script successfully\ninstalled Docker CE by pulling and running an hello world app."
echo ""
echo "To exit, press Ctrl+C or Ctrl+D."
echo "####################################"
echo ""

# Asks whetever if you want to upgrade your system.
read -p "Would do you like to start updating your system first? [y/n]" updateSystemFirst
if [ $updateSystemFirst = "y"]
then
    sudo apt update
    sudo apt upgrade
else
    echo 'You skipped the upgrade process. Upgrde later to keep safe.'
fi

echo "We're running kernel checks first to make sure you can troubleshoot later."
curl https://raw.githubusercontent.com/docker/docker/master/contrib/check-config.sh > check-config.sh | bash -

# Install dependencies first before pulling Docker's public keys
echo "We're installing some things for this script to successfully install Docker..."
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common

echo "We're now finished downloaded the required things. Now, getting the Docker's key public key..."
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88

# Next, add
echo "Adding Docker repositories to known APT repositories..."
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable"
sudo apt-get update

echo "####################################"
echo ""
echo "Installing Docker..."
echo ""
echo "Installation process may take a little bit longer."
echo ""
echo "####################################"
sudo apt-get install docker-ce

echo "####################################"
echo ""
echo "Adding your account to 'docker' group..."
echo ""
echo "We have been warned you! Adding any other users to this group will grant prvilleges simliar to root user, so please be careful!\n\nSee https://docs.docker.com/v17.09/engine/security/security/#docker-daemon-attack-surface for details."
echo ""
echo "####################################"
sudo groupadd docker
sudo usermod -aG docker $USER

sudo systemctl enable docker

echo "####################################"
echo ""
echo "Running hello-world package..."
echo ""
echo "We'll run the hello-world container for you to confirm Docker installation."
echo ""
echo "####################################"
docker run hello-world

echo "####################################"
echo ""
echo "Thank you for hanging out on using the script!"
echo ""
echo "Decluttering your console so you can start Docker now. Thanks again."
echo ""
echo "####################################"
clear
