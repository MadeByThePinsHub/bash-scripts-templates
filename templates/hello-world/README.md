# Hello World Scripts

This set of templates contains the Hello World scripts and messages.

## Sudo What?

There are some scripts that required superpowers from `sudo`, 

```bash
## 
$ sudo bash $HOME/bash-scripts-temps/templates/hello-world/hello-world-docker_ce.sh
```
