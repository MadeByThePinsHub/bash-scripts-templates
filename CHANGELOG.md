# Changelogs and Release Schedules FAQs
This file contains the things happened in this repository. Updates every week, so keep your eyes peeled here. Dates are in Philippine Standard Time and versioning follows [Semantic Versioning](https://semver.org)

## Guide To Semantic Versioning

To identify whetever the templates repository is available for public use, use this guide below to identify
* **Major Release** - Major releases are thesewhere there are major changes like new categories, among others.
* **Mirror Releases** - Mirror releases are these where we add more templates for specific templates.
* **Patch Releases** - Patch Releases are these where we fixed those quirks reported and documented in our GitLab Issue Tracker.
* **Release Channels** - Tells if the the release is `stable`, `rc` (Release candidates can be also numbered when there are revisions on it, like `v0.0.1-rc-1` and so on), `beta` or `alpha`.

Numbering versions should be lie mj.mr.pt-release_channel whereas `mj` is the Major Release Number, while `mr` is Mirror Release, `pt` should be Patch Release, and `release_channel` is the Release Channel. Stable releases can also be without the `-stable` suffix.

## Release Schedules
There is higher chance that we might not follow the release schedule
| Release Channel | Release Schedule | Description | Release Branch | Latest Release |
| --------------- | --------- | ----------- | ------------------ | -------------- |
| `edge` / `canary` | Continous | Continous development testing, mostly unstable | `master` | see git commits |
| `alpha`         | Every 4 weeks | Unstable release, still under testing but starts a new release | Mostly on `master`, checked out and tagged | 0.0.1-alpha (TBA)
| `beta` | Every 8 weeks, schedules may varies | TBA |
| `rc` | Every 3 months | Candidates for stable releases are under rigrious testing and review. | Checked out and tagged | TBA |
| `stable` or `release` | Twice a year | Official stable release when cerfitied in compliance. | `stable` | TBA |

## Changelogs
The changelog below are one commit behind the latest commit activity because we waiting for the hash. If you want to verify if that commiter owns that, we are using 

### Latest Version 0.0.1-alpha - Feburary 1, 2020-TBA [First Patch Release in Alpha Release Channel]

* Getting Things Started (Commited by @AndreiJirohHaliliDev2006 with commit ID `cf212e45e586405092605a14edecd9e926deff4e`)
* Released Contributors' Documentation (Commited by @AndreiJirohHaliliDev2006 with commit ID `df8d5d94d810e36ed7c41e064210326f04cb6ed6`)
* Another business done (Commited by @AndreiJirohHaliliDev2006 with commit ID `282723347d07c772cdf4672ef002abdc62e4c664`)
