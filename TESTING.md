# Community Script Testers Program
If you tested the scripts we and the community made and want to be included in this list, please create an new issue using the **Community Script Testers Inquiry - Include My Name** template.

## Community Testers Directory
> Also mirrored on [this directory from our team handbook]

| Name | GitLab Handle | Keybase | Testing scripts since... |
| ---- | ------------- | ------- | ------------------------ |
| TBA | TBA | TBA | TBA |

[this directory from our team handbook]: https://en.handbooksbythepins.gq/directory/community-testers/bash-scripts-templates

## Script Testing Documentation
Our team handbook has an extensive documentation on how you can test our scripts without making changes to your computer or even nuking everything.

* [Setting up Virtual Environment](https://en.handbooksbythepins.gq/devops/verify/virtual-env/virtual-machines/set-up)
  * [Setting up Linux VM](https://en.handbooksbythepins.gq/devops/verify/virtual-env/virtual-machines/set-up#installing-ubuntu-in-vm)
  * [Guest Additions 101](https://en.handbooksbythepins.gq/devops/verify/vitual-env/virtual-machines)
